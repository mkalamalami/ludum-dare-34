var http = require('http');
var url = require('url');
var querystring = require('querystring');
var db = require('./db');

const PORT = 8034;
db.loadFromDisk();

var server = http.createServer(function (request, response) {
	try {
		var queryRaw = url.parse(request.url).query;
		if (queryRaw) {
			var query = querystring.parse(queryRaw);
			if (query.callback && query.data && query.data.length > 0) {
				var data = query.data;
				for (var i = 0; i < 3; i++) {
					data = new Buffer(data, 'base64').toString('ascii');
				}
				data = JSON.parse(data);
				db.registerScore(data.name, data.score, data.timestamp);
				response.writeHead(200, {
				  'Content-Type': 'text/javascript',
				  'Access-Control-Allow-Origin': '*'
				});
				response.end(query.callback + "()");
			}
			else if (query.callback) {
				var data = db.getLeaderboard();
				response.writeHead(200, {
				  'Content-Type': 'text/javascript',
				  'Access-Control-Allow-Origin': '*'
				});
				response.end(query.callback + "(" + JSON.stringify(data) + ")");
			}
			else {
				if (request.url != '/' && request.url != '/favicon.ico') {
					console.warn("failed request: " + err.message + " (" + request.url + ")");
				}
				response.writeHead(400, {
				  'Access-Control-Allow-Origin': '*'
				});
				response.end();
			}
		}
		else {
			var html = '<html><head><title>Squ Leaderboard</title><style type="text/css">body { font-family: arial; text-align: center } a { color: black } table { margin: auto } td { min-width: 200px; font-size: 20px; color: red; } </style><meta charset="utf-8" /></head><body><img src="http://marwane.kalam-alami.net/ld34/img/logo.png" /><p><a href="http://ludumdare.com/compo/ludum-dare-34/?action=preview&uid=9172">Ludum Dare entry page</a><h1>Leaderboard</h1></p><table>';
			db.getLeaderboard().forEach(function(row) {
				html += '<tr><td style="text-align: right; padding-right: 30px; ">' + row.name +'</td><td style="text-align: left; padding-left: 30px; font-weight: bold">' + row.score + '</td></tr>';
			});
			html += '</table>';
			response.writeHead(200, {
			  'Content-Type': 'text/html'
			});
			response.end(html);
		}
	}
	catch (err) {
		console.warn("failed request: " + err.message + " (" + request.url + ")");
		response.writeHead(400, {
		  'Access-Control-Allow-Origin': '*'
		});
		response.end();
	}
});

// Listen on port 8000, IP defaults to 127.0.0.1
server.listen(PORT);

console.log("running on port " + PORT);

