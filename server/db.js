var fs = require('fs');

const DB_FILE = 'leaderboard.json';
const MAX_SIZE = 10;

var leaderboardCache = [
	/* {name,score,timestamp} */
];

exports.loadFromDisk = function() {
	var fileContents = '';
	try {
		fileContents = fs.readFileSync(DB_FILE);
	}
	catch (err) {
		console.log(err.message);
	}

	if (fileContents.length > 0) {
		try {
			leaderboardCache = JSON.parse(fileContents.toString());
			console.log("loaded from disk");
		}	
		catch (err) {
			console.error(err.message);
			console.info("load failed, making backup");
			fs.renameSync(DB_FILE, DB_FILE + '.' + Date.now());
		}
	}
	else {
		console.log("disk file empty, using fresh database");
	}
};

exports.getLeaderboard = function() {
	var data = [];
	leaderboardCache.forEach(function(row) {
		data.push({
			name: row.name,
			score: row.score
		})
	})
	return data;
};

exports.registerScore = function(name, score, timestamp) {
	var minimalScore = 0;
	if (leaderboardCache.length >= MAX_SIZE) {
		minimalScore = leaderboardCache[leaderboardCache.length - 1].score;
	}

	if (score > minimalScore) {
		var insertIndex = leaderboardCache.length;
		for (var i = 0; i < leaderboardCache.length; i++) {
			if (score > leaderboardCache[i].score && i < insertIndex) {
				insertIndex = i;
			}
			if (name == leaderboardCache[i].name && timestamp == leaderboardCache[i].timestamp) {
				console.log("replay ignored: " + name + " / " + score + " / " + timestamp);
				return;
			}
		}
		console.log("new high score: " + score + " by " + name + " (ranked " + (insertIndex+1) + ")");
		leaderboardCache.splice(insertIndex, 0,
			{name: name, score: score, timestamp: timestamp});
		if (leaderboardCache.length >= MAX_SIZE) {
			leaderboardCache.splice(10, leaderboardCache.length - MAX_SIZE);
		}
		this._saveToDisk();
	}
};

exports._saveToDisk = function() {
	fs.writeFile(DB_FILE, JSON.stringify(leaderboardCache), function(err) {
		if (err) {
			console.error(err.message);
		}
		else {
			console.log("saved to disk");
		}
	})
};
