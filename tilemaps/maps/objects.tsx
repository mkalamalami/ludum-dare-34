<?xml version="1.0" encoding="UTF-8"?>
<tileset name="objects" tilewidth="96" tileheight="96" tilecount="100">
 <image source="../tiles/objects.png" width="960" height="960"/>
 <tile id="0">
  <properties>
   <property name="type" value="player"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="name" value="bush"/>
   <property name="type" value="monster"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="destructible" value="true"/>
   <property name="name" value="stump"/>
   <property name="type" value="obstacle"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="destructible" value="true"/>
   <property name="name" value="stumpbig"/>
   <property name="type" value="obstacle"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="destructible" value="false"/>
   <property name="name" value="wall"/>
   <property name="small_passage" value="true"/>
   <property name="type" value="obstacle"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="name" value="acorn"/>
   <property name="type" value="point"/>
   <property name="value" value="10"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="name" value="mystic1"/>
   <property name="type" value="fruit"/>
   <property name="value" value="1"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="name" value="mystic2"/>
   <property name="type" value="fruit"/>
   <property name="value" value="2"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="name" value="mystic3"/>
   <property name="type" value="fruit"/>
   <property name="value" value="3"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="name" value="mystic4"/>
   <property name="type" value="fruit"/>
   <property name="value" value="4"/>
  </properties>
 </tile>
</tileset>
