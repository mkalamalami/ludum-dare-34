define(['consts'], function(Consts) {
    
    var DEFAULT_STANCE = "walk";

	return {
		create: create
	};
    
    function create(player) {

        var character = {
            player : player,
            id : 1,
            name : "Squ",
            sound: "call-squ",
            help : { action : "Jump"},
            currentAnimation : null,
            currentTerrain : null,
            isFalling : false
        };
        // Stances

        character.stances = {
            "walk": {
                texture: "squ-walk",
                body: {
                    width: 70,
                    height: 50,
                    offset: {
                        x: 20
                    }
                },
                animations: [
                    {name: "walk"}
                ],
                speedWaterScale: 0.7
            },
            "jump": {
                texture: "squ-jump",
                body: {
                    width: 70,
                    height: 50,
                    offset: {
                        x: 20,
                        y: -5
                    }
                },
                animations: [
                    {name: "jump", frames : [0,1]},
                    {name: "fall", frames : [2,3]}
                ],
                speedWaterScale: 0.7
            }
        };
        
        // Activation

        character.activate = function() {
            character.currentAnimation = null;
            character.currentTerrain = null;
            character.isFalling = false;
            this.player.switchStance(this.stances[DEFAULT_STANCE]);
        }

        // Update

        character.update = function(action, terrain) {
            var player = this.player;
            if (terrain != this.currentTerrain) {
                // Changement de terrain => Changement animation
                this.currentTerrain = terrain;
                if (terrain != Consts.TERRAIN.AIR) {
                    player.switchStance(this.stances["walk"]);
                    player.animations.play("walk", 10, true);
                }
                else {
                    player.switchStance(this.stances["jump"]);
                    if (player.body.velocity.y >= 0) {
                        player.animations.play("fall", 10, true);
                        this.isFalling = true;
                    }
                    else {
                        player.animations.play("jump", 10, true);
                        this.isFalling = false;
                    }
                }
            } else if (terrain == Consts.TERRAIN.AIR
                    && !this.isFalling && player.body.velocity.y >= 0) {
                player.animations.play("fall", 10, true);
                this.isFalling = true;
            }

            // Run action
            if (action) {
                launchCharacterAction(player, terrain);
            }
        };
        
        return character;
    }
    
    function launchCharacterAction(player, terrain) {
        // jump
        if (checkIfCanJump(player)) {
                player.body.moveUp(Consts.JUMP_FORCE);
                player.animations.play("jump", 5, false);
            }
    }

    function checkIfCanJump(player) {
        game = player.game;
        var yAxis = p2.vec2.fromValues(0,1);
        var result = false;
    
        for (var i=0; i < game.physics.p2.world.narrowphase.contactEquations.length; i++)
        {
            var c = game.physics.p2.world.narrowphase.contactEquations[i];
    
            if (c.bodyA === player.body.data || c.bodyB === player.body.data)
            {
                var d = p2.vec2.dot(c.normalA, yAxis);
                if (c.bodyA === player.body.data)
                {
                    d *= -1;
                }
    
                if (d > 0.5)
                {
                    result = true;
                }
            }
        }
        return result;
    }
    
});
