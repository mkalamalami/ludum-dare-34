define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};

	function create(state, data) {
		
		var monster = state.game.add.sprite(data.x, data.y, data.properties.name);
		if (data.properties.scale) {
            monster.scale = new PIXI.Point(data.properties.scale, data.properties.scale);
        }
		
		monster.info = {
			direction : data.properties.direction || "left",
			invincible: data.properties.invincible == "true"
		};
		
		state.game.physics.p2.enable(monster, Consts.DEBUG.SHOW_BODIES);
		monster.body.fixedRotation = true;
		monster.body.setCollisionGroup(state.groups.enemies);
		monster.body.collides(state.groups.player);
		monster.body.collides(state.groups.map);

		// Animation
		monster.animations.add('walk');
		monster.animations.play('walk', 15, true);
		
		// correction position body
		Utils.correctObjectPosition(monster,true, Consts.TILESIZE);
		
		if (data.properties.gravity) {
            monster.body.data.gravityScale = parseInt(data.properties.gravity);
        }
		
		// Audio

		// IA
		monster.update = function() {
			if (this.dead) {
				this.alpha -=0.02;
				if (this.alpha == 0) {
                    this.destroy();
                }
            }
		};
		
		monster.death = function(){
			this.body.moveUp(500);
			this.body.clearShapes();
			this.dead = true;
		}
		return monster;
	}
});
