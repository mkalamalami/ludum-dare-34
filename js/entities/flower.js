define(['consts'], function(Consts) {

	var SCALE_FACTOR = 0.002;
	var SHOW_FLOWER_2 = 0.3;
	var SHOW_FLOWER_3 = 0.45;
	var SHOW_FLOWER_MAX = 0.6;

	var Flower = function(state) {
		this._state = state;
		this.group = state.add.group();
		this._sprites = [];
		for (var i = 1; i <= 3; i++) {
			var sprite = state.add.image(0, 0, 'flower' + i);
			sprite.anchor.set(0.5, 1);
			if (i != 1) {
				sprite.alpha = 0;
			}
			this.group.add(sprite);
			this._sprites.push(sprite);
		}
		this.group.x = Consts.WIDTH/2;
		this.group.y = 430;
		this.group.scale.set(0.1);
	}

	Flower.prototype.tween = function(targetScale, duration, callback) {
		var flowerBirth = this._state.add.tween(this.group.scale)
			.to({ x: targetScale, y: targetScale }, duration, Phaser.Easing.Quadratic.Out);
		if (callback) {
			flowerBirth.onComplete.addOnce(callback);
		}
		flowerBirth.onComplete.addOnce(this._refreshAlpha, this, 0, true);
		flowerBirth.start();
		return flowerBirth;
	}

	Flower.prototype.grow = function() {
		this._offsetScale(SCALE_FACTOR);
		this._refreshAlpha();
	}

	Flower.prototype.shrink = function() {
		this._offsetScale(-SCALE_FACTOR);
		this._refreshAlpha();
	}

	Flower.prototype._refreshAlpha = function(toggleMode) {
		var offset = toggleMode ? 1 : 0.1;

		if (this._getScale() > SHOW_FLOWER_2 && this._sprites[1].alpha < 1) {
			this._sprites[1].alpha = Math.min(1, this._sprites[1].alpha+offset);
		}
		if (this._getScale() > SHOW_FLOWER_3 && this._sprites[2].alpha < 1) {
			this._sprites[2].alpha = Math.min(1, this._sprites[2].alpha+offset);
		}

		if (this._getScale() < SHOW_FLOWER_2 && this._sprites[1].alpha > 0) {
			this._sprites[1].alpha = Math.max(0, this._sprites[1].alpha-offset);
		}
		if (this._getScale() < SHOW_FLOWER_3 && this._sprites[2].alpha > 0) {
			this._sprites[2].alpha = Math.max(0, this._sprites[2].alpha-offset);
		}
	}

	Flower.prototype.isReadyToRock = function() {
		return this._getScale() >= SHOW_FLOWER_MAX;
	}

	Flower.prototype._getScale = function() {
		return this.group.scale.x;
	}

	Flower.prototype._offsetScale = function(offset) {
		var newScale = this._getScale() + offset;
		newScale = Math.min(SHOW_FLOWER_MAX, Math.max(0, newScale));
		this.group.scale.set(newScale);
	}

	return {
		create: function(state) {
			return new Flower(state);
		}
	};
});
