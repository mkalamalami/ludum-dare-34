define(['consts'], function(Consts) {
	return {
		create: create
	};

	function create(state, data) {
		
		var fruit = state.game.add.sprite(data.x, data.y, data.properties.name);
		if (data.properties.scale) {
            fruit.scale = new PIXI.Point(data.properties.scale, data.properties.scale);
        }

		state.game.physics.p2.enable(fruit, Consts.DEBUG.SHOW_BODIES);
		fruit.body.fixedRotation = true;
		fruit.body.setCollisionGroup(state.groups.fruits);
		fruit.body.collides(state.groups.player);
		fruit.body.collides(state.groups.map);
		
		// correction position body
		fruit.body.x += fruit.width/2;
		fruit.body.y -= fruit.height/2;
		
		fruit.info = {
			initialY : fruit.body.y,
			state : state,
			captured: false,
			value: parseInt(data.properties.value)
		}
		
		// Audio

		// IA
		fruit.update = function() {
			if (this.info.captured) {
				if (this.info.initialY - fruit.body.y < 200) {
                    this.body.velocity.y -= 50;
                } else {
					this.body.velocity.y = 0;
					this.alpha -=0.05;
					if (this.alpha == 0) {
						this.info.state.notifyCaptureFruit(false);
						this.destroy();
					}
				}
            }
		};
		
		fruit.capture = function(){
			// TODO
			if (!this.info.captured) {
                this.body.clearShapes();
				this.info.captured = true;
				this.info.state.notifyCaptureFruit(true, this.info.value);
            }

		}
		
		return fruit;
	}
});
