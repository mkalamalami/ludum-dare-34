define(['consts'], function(Consts) {
    
	return {
		create: create
	};
    
    function create(player) {
        var character = {
            player : player,
            id : 3,
            name : "Delta Squ",
            sound: "call-delta",
            help : {
                passive : "Can Glide"
            },
            currentAnimation : null,
            currentTerrain : null,
            isFalling : false
        };

        character.stances = {
            "walk": {
                texture : "delta-walk",
                body: {
                    width: 70,
                    height: 50,
                    offset: {
                        x: 20
                    }
                },
                animations : [
                    {name : "walk" }
                ],
               // speedGroundScale: 0.7,
                speedWaterScale: 0.2
            },
            "fly": {
                texture : "delta-fly",
                body: {
                    width: 70,
                    height: 50,
                    offset: {
                        x: 20,
                        y: -10
                    }
                },
                animations : [
                    {name : "fly" }
                ],
              //  speedGroundScale: 0.7,
                gravityScale:0.1,
                speedWaterScale: 0.2
            }
        };
        
        character.activate = function() {
            this.currentAnimation = null;
            this.currentTerrain = null;
            this.isFalling = false;
            player.body.velocity.y = 0; // planer
            this.player.switchStance(this.stances["walk"]);
        }

        character.update = function(action, terrain) {
            if (terrain != this.currentTerrain || this.currentAnimation == null) {
                this.currentTerrain = terrain;
                if (terrain != Consts.TERRAIN.AIR) {
                    this.player.switchStance(this.stances["walk"]);
                    this.player.animations.play("walk", 7, true);
                } else {
                    this.player.switchStance(this.stances["fly"]);
                    this.player.animations.play("fly", 7, true);
                }
            }
        };

        return character;
    }
    
    function launchCharacterAction(player) {
    
    }
    
});
