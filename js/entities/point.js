define(['consts'], function(Consts) {
	return {
		create: create
	};

	function create(state, data) {
		var point = state.game.add.sprite(data.x, data.y, data.properties.name);
		if (data.properties.scale) {
            point.scale = new PIXI.Point(data.properties.scale, data.properties.scale);
        }
		
		state.game.physics.p2.enable(point, Consts.DEBUG.SHOW_BODIES);
		point.body.fixedRotation = true;
		point.body.setCollisionGroup(state.groups.points);
		point.body.collides(state.groups.player);
		point.body.collides(state.groups.map);
		
		data.properties.value = parseInt(data.properties.value);
		
		point.info = data;
		
		// correction position body
		point.body.x += point.width/2;
		point.body.y -= point.height/2;
		
		if (data.properties.gravity) {
            point.body.data.gravityScale = parseInt(data.properties.gravity);
        }

		// Animation
		
		// Audio

		point.update = function() {
		};
		
		point.getValue = function(){
			return data.properties.value;
		}

		return point;
	}
});
