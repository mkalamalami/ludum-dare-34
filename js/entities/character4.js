define(['consts'], function(Consts) {
    
	return {
		create: create
	};
    
    function create(player) {
        var character = {
            player : player,
            id : 4,
            name : "Metal Squ",
            sound: "call-metal",
            help : {
                action : "Roll to destroy"
            },
            currentAnimation : null,
            currentTerrain : null,
            isFalling : false
        };

        character.stances = {
            "walk": {
                texture : "steel-walk",
                body: {
                    width: 90,
                    height: 70,
                    offset: {
                        x: 30
                    }
                },
                animations : [
                    {name : "walk" }
                ],
              //  speedGroundScale: 0.7,
                speedWaterScale: 0
            },
            "brawl": {
                texture : "steel-brawl",
                body: {
                    width: 96,
                    height: 96,
                    offset: {
                        x: 0
                    }
                },
                animations : [
                    {name : "brawl" }
                ],
                //speedGroundScale: 0.7,
                gravityScale: 5,
                speedWaterScale: 0,
                killEnemyOnHit: true,
                destructor: true
            }
        };
        
        character.activate = function() {
            this.currentAnimation = null;
            this.currentTerrain = null;
            this.isFalling = false;
            this.nextChange = 0;
            this.player.switchStance(this.stances["walk"]);
        }

        character.update = function(action, terrain) {
            var expectedAnimation = 'walk';
            if (terrain == Consts.TERRAIN.AIR || action) {
                expectedAnimation = 'brawl';
            }
            if (this.currentAnimation != expectedAnimation && this.player.game.time.now > this.nextChange) {
                this.player.switchStance(this.stances[expectedAnimation]);
                this.player.animations.play(expectedAnimation, expectedAnimation == 'brawl' ? 20 : 7, true);
                this.currentAnimation = expectedAnimation;
                this.nextChange = this.player.game.time.now + 500;
            }
        };

        return character;
    }
    
    function launchCharacterAction(player) {
    
    }
    
});
