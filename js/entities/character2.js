define(['consts'], function(Consts) {
    
	return {
		create: create
	};
    
    function create(player) {
        var character = {
            player : player,
            id : 2,
            name : "Foxy Squ",
            sound: "call-foxy",
            help : {
                action : "Hold &#x21E6; to roll below rocks",
                passive : "Good swimmer"
            },
            currentAnimation : null,
            currentTerrain : null,
            isFalling : false
        };

        character.stances = {
            "walk": {
                texture : "foxy-walk",
                body: {
                    width: 50,
                    height: 50,
                    offset: {
                        x: 15
                    }
                },
                animations : [
                    {name : "walk" },
                    {name : "jump", frames : [2,3]}
                ],
                speedGroundScale: 0.8,
                speedWaterScale: 1,
                gravityScale:1
            },
            "roll": {
                texture : "foxy-roll",
                body: {
                    width: 50,
                    height: 50,
                    offset: {
                        x: 0
                    }
                },
                animations : [
                    {name : "roll" }
                ],
                speedGroundScale: 0.8,
                speedWaterScale: 1,
                gravityScale:1,
                small: true
            }
        };
        
        character.activate = function() {
            this.currentAnimation = null;
            this.currentTerrain = null;
            this.isFalling = false;
            this.player.switchStance(this.stances["default"]);
        }

        character.update = function(action, terrain) {
            var player = this.player;

            if (action) {
                player.switchStance(this.stances["roll"]);
                player.animations.play("roll", 20, true);
            }
            else {
                player.switchStance(this.stances["walk"]);
                if (terrain != this.currentTerrain || this.currentAnimation == null) {
                    // Changement de terrain => Changement annimation
                    this.currentTerrain = terrain;
                    if (terrain != Consts.TERRAIN.AIR) {
                        player.animations.play("walk", 10, true);
                    } else {
                        player.animations.play("jump", 10, true);
                    }
                }
            }
        };
        
        return character;
    }

});
