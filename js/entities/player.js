define(['consts','engine/save-state' , 'entities/character1','entities/character2','entities/character3','entities/character4'],
	   function(Consts, SaveState, CharacterManager1, CharacterManager2, CharacterManager3, CharacterManager4) {

	return {
		preload: preload,
		create: create,
		destroy: destroy
	};

	function preload(state) {
		//state.game.load.physics('physicsDataPlayer', 'img/spritesheet/player-physics.json');
	}
	
	function create(state, playerInfo) {
		
		var player = state.game.add.sprite(playerInfo.x, playerInfo.y, 'squ-walk');
				
		// Physics
		state.game.physics.p2.enable(player, Consts.DEBUG.SHOW_BODIES);
		player.body.fixedRotation = true;
		
		// correction position body
		player.body.x += player.width/2;
		player.body.y -= player.height/2;
		
		player.info = {};
		player.info.state = state;
		player.info.activeCharacter = null;
		player.info.points = SaveState.points || 0;
		player.info.lockStanceUntil = 0;
		player.info.characters = [];
		player.info.characters[1] = CharacterManager1.create(player);
		player.info.characters[2] = CharacterManager2.create(player);
		player.info.characters[3] = CharacterManager3.create(player);
		player.info.characters[4] = CharacterManager4.create(player);
		
		// Keyboard binding

		var keyboard = state.game.input.keyboard;
		keyboard.addKey(Phaser.Keyboard.ONE);
		keyboard.addKey(Phaser.Keyboard.TWO);
		keyboard.addKey(Phaser.Keyboard.THREE);
		keyboard.addKey(Phaser.Keyboard.FOUR);
		keyboard.addKey(Phaser.Keyboard.NUMPAD_1);
		keyboard.addKey(Phaser.Keyboard.NUMPAD_2);
		keyboard.addKey(Phaser.Keyboard.NUMPAD_3);
		keyboard.addKey(Phaser.Keyboard.NUMPAD_4);

		// Input
		player.updateData = function(camera) {
			// Switch character

			if (!this.info.activeCharacter) {
				this.changeCharacter(1);
			}
			
			var terrain = this.getCurrentTerrain();
			var action = false;
			
			if (this.info.state.game.time.now > this.info.lockStanceUntil) {
				if (keyboard.isDown(Phaser.KeyCode.UP) || keyboard.isDown(Phaser.KeyCode.ONE) || keyboard.isDown(Phaser.KeyCode.NUMPAD_1)) {
					if (this.info.state.levelInfo.unlockedCharacters >= 1) {
						player.changeCharacter(1);
						action = true;
					}
				} else if (keyboard.isDown(Phaser.KeyCode.LEFT) || keyboard.isDown(Phaser.KeyCode.TWO) || keyboard.isDown(Phaser.KeyCode.NUMPAD_2)) {
					if (this.info.state.levelInfo.unlockedCharacters >= 2) {
						player.changeCharacter(2);
						action = true;
					}
				} else if (keyboard.isDown(Phaser.KeyCode.RIGHT) || keyboard.isDown(Phaser.KeyCode.THREE) || keyboard.isDown(Phaser.KeyCode.NUMPAD_3)) {
					if (this.info.state.levelInfo.unlockedCharacters >= 3) {
						player.changeCharacter(3);
						action = true;
					}
				} else if (keyboard.isDown(Phaser.KeyCode.DOWN) || keyboard.isDown(Phaser.KeyCode.FOUR) || keyboard.isDown(Phaser.KeyCode.NUMPAD_4)) {
					if (this.info.state.levelInfo.unlockedCharacters >= 4) {
						player.changeCharacter(4);
						action = true;
					}
				}
			}

            // Move according to character properties
			
			var character = this.currentCharacter();
			character.update(action, terrain);
			
			var speedScale = 0;
			if (terrain == Consts.TERRAIN.GROUND) {
				if (this.lastWall > this.info.state.game.time.now - 500) {
					speedScale = 1.4; // roll fast through the wall
				}
				else {
					speedScale = this.currentStance.speedGroundScale || 1;
				}
				this.lastSolidTerrain = terrain;
				this.lastWave = null;
			} else if (terrain == Consts.TERRAIN.WATER) {
				speedScale = this.currentStance.speedWaterScale || 1;
				this.lastSolidTerrain = terrain;

				var time = this.info.state.game.time.now;
				if (!this.lastWave || time - this.lastWave > 500) {
					this.lastWave = time;
					var waves = this.info.state.add.image(this.x - this.width / 3, this.y, 'waves');
					this.info.state.add.tween(waves)
						.to({alpha: 0}, 300)
						.start();
				}

			} else if (terrain == Consts.TERRAIN.AIR) {
				this.lastWave = null;
				if (this.currentStance.speedAirScale) {
					speedScale = this.currentStance.speedAirScale;
				}
				else if (this.lastSolidTerrain == Consts.TERRAIN.WATER) {
					speedScale = this.currentStance.speedWaterScale || 1;
				}
				else if (this.lastSolidTerrain == Consts.TERRAIN.GROUND) {
					speedScale = this.currentStance.speedGroundScale || 1;
				}
				else {
					speedScale = 1;
				}
			}
			
			if ((camera.body.x - player.body.x) > 180 && speedScale == 1) {
                speedScale += Consts.LATE_PLAYER_ACCELERATION;
            }
		/*	if (player.body.stuck) {
                speedScale = 0;
            }*/
			
			
			this.body.velocity.x = Consts.SPEED * speedScale;
			
		};
		
		player.changeCharacter = function(id, force){
			if (this.info.activeCharacter != id && !this.info.state.pauseCharacter) {
				this.currentStance = null;

				if (this.info.activeCharacter) {
					smokeAnimation(this.info.state, this);
				}
				this.info.activeCharacter = id;
				var character = this.currentCharacter();
				character.activate();
				this.info.state.notifyChangeCharacter();
			}
		}

		player.switchStance = function(stance) {
			var character = this.currentCharacter();
			if (stance != this.currentStance) {
				this.currentStance = stance;
				if (this.currentStance.small) {
					this.lastRollAttempt = this.info.state.game.time.now;
				}
				player.body.stuck = false;

				var posX = this.body.x;
				var posY = this.body.y;
			
				this.loadTexture(stance.texture, 0, true);
				stance.animations.forEach(function(animation) {
					this.animations.add(animation.name, animation.frames || null);
				}, this);
		
				// Physics
				//this.body.addPolygon( {} , stance.polygon);
				this.body.clearShapes();
				this.body.addRectangle(stance.body.width,stance.body.height,stance.body.offset.x || 0, stance.body.offset.y || 0);
				
				this.body.x = posX;
				this.body.y = posY;

				this.body.setCollisionGroup(state.groups.player);
				this.body.collides(state.groups.enemies, collideWithEnemy, this);
				this.body.collides(state.groups.points, collectPoints, this);
				this.body.collides(state.groups.fruits, collideWithFruit, this);
				this.body.collides(state.groups.obstacles, collideWithObstacle, this);
				this.body.collides(state.groups.map);
				
				this.body.data.gravityScale = stance.gravityScale || 1;
			}
		}
		
		player.getCurrentTerrain = function(){
			var contactEquations = this.game.physics.p2.world.narrowphase.contactEquations;
			var yAxis = p2.vec2.fromValues(0,1);
			var result = false;
		
			for (var i=0; i < contactEquations.length; i++) {
				var c = contactEquations[i];
				// If it's a player collision and with something else than the world borders
				if ((c.bodyA === this.body.data || c.bodyB === this.body.data) && (c.bodyA.parent != null && c.bodyB.parent != null))
				{
					var d = p2.vec2.dot(c.normalA, yAxis);
					var terrain;
					if (c.bodyA === player.body.data){
						d *= -1;
						terrain = c.bodyB.parent.terrain;
					} else {
						terrain = c.bodyA.parent.terrain;
					}
		
					if (d > 0.5) {
						return terrain;
					}
				}
			}
			// Player in the air if nothing else
			return Consts.TERRAIN.AIR;
		}
		
		player.currentCharacter = function() {
			return this.info.characters[this.info.activeCharacter];
		}
		
		player.death = function(){
			this.dead = true;
			player.info.state.death();
		}
		return player;
	}
	
	function collideWithEnemy(playerBody, enemyBody){
		if (enemyBody && enemyBody.sprite.death != true) {
			this.info.state.add.tween(this)
				.to({rotation: -10, alpha: 0, x: this.x - 300, y: this.y - 100}, 1000)
				.start();
			if (!enemyBody.sprite.info.invincible && this.currentStance.killEnemyOnHit) {
				enemyBody.sprite.death();
			} else {
				this.death();
			}
		}
	}
	
	function collideWithObstacle(playerBody, obstacleBody){
		if (obstacleBody && obstacleBody.sprite.info.small_passage == true) {
			if (playerBody.sprite.getCurrentTerrain() == Consts.TERRAIN.GROUND
			&& (this.currentStance.small || this.lastRollAttempt && this.lastRollAttempt > this.info.state.game.time.now - 500)) {
				obstacleBody.sprite.allowPassageBelow();
				this.lastWall = this.info.state.game.time.now;
				this.lockStanceUntil = this.info.state.game.time.now + 500;
			} else {
				playerBody.stuck = true;
				playerBody.x -= 25;
			}
		} else if (obstacleBody && obstacleBody.sprite.info.destructible == true) {
			if (this.currentStance.destructor) {
				obstacleBody.sprite.explode();
			} else {
				playerBody.stuck = true;
				playerBody.x -= 25;
			}
		} else {
			playerBody.x -= 25;
			playerBody.stuck = true;
		}
	}
	
	function collectPoints(playerBody, pointBody){
		if (pointBody && pointBody.sprite) {
			this.info.points += pointBody.sprite.getValue();
			this.info.state.collectNewPoints(this.info.points);
			pointBody.sprite.destroy();
			flashAnimation(this.info.state, pointBody, 0.2);
		}
	}

	function collideWithFruit(playerBody, fruitBody){
		if (fruitBody && fruitBody.sprite.info.captured != true) {
			fruitBody.sprite.capture();
			flashAnimation(this.info.state, fruitBody);
		}
	}

	function flashAnimation(state, sprite, scale) {
		// animation
		if (!SaveState.mute) {
			state.add.sound(scale >= 0.3 ? 'start' : 'click').play();
		}
		var flash = state.add.sprite(sprite.x, sprite.y, 'flash');
		flash.anchor.set(0.5);
		flash.scale.set(scale || 0.3);
		state.add.tween(flash)
			.to({alpha: 0, rotation: 50}, 500)
			.start();
	}

	function smokeAnimation(state, player) {
		if (!SaveState.mute) {
		//	state.add.sound('start).play();
		}
		var smoke = state.add.sprite(player.x, player.y, 'smoke');
		smoke.anchor.set(0.5);
		smoke.scale.set(player.width/smoke.width);
		state.add.tween(smoke)
			.to({alpha: 0, rotation: 50}, 300)
			.start();
		state.add.tween(smoke.scale)
			.to({x: smoke.scale.x * 1.5, y: smoke.scale.x * 1.5}, 300)
			.start();
	}
		

	function destroy(player) {
	};

});
