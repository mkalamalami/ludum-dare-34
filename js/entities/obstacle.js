define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};

	function create(state, data) {
		
		var obstacle = state.game.add.sprite(data.x, data.y, data.properties.name);
		if (data.properties.scale) {
            obstacle.scale = new PIXI.Point(data.properties.scale, data.properties.scale);
        }

		state.game.physics.p2.enable(obstacle, Consts.DEBUG.SHOW_BODIES);
		obstacle.body.fixedRotation = true;		
		
		// correction position body
		Utils.correctObjectPosition(obstacle, true, Consts.TILESIZE);
		
		if (data.properties.customcollision) {
			obstacle.body.clearShapes();
			var collisionRect = Consts.COLLISION[data.properties.name];
            obstacle.body.addRectangle(collisionRect.width,collisionRect.height,collisionRect.offset.x, collisionRect.offset.y);
			obstacle.body.y += -collisionRect.offset.y + (obstacle.height - collisionRect.height) / 2;
        }
		
		obstacle.body.setCollisionGroup(state.groups.obstacles);
		obstacle.body.collides(state.groups.player);
		obstacle.body.collides(state.groups.map);
		
		obstacle.body.kinematic = true;
		
		obstacle.info = {
			destructible: data.properties.destructible == "true" || false,
			small_passage: data.properties.small_passage == "true" || false
		}
		
		
		// Audio

		// IA
		obstacle.update = function() {
			if (this.destroyed) {
				this.alpha -=0.02;
				if (this.alpha == 0) {
                    this.destroy();
                }
            }
		};
		
		obstacle.explode = function(){
			// TODO 
			this.body.clearShapes();
			this.destroyed = true;
		}
		
		obstacle.allowPassageBelow = function(){
			// TODO 
			this.body.clearShapes();
			// this.body.kinematic = true;
		}
		return obstacle;
	}
});
