define(['lib/moment.min', 'consts', 'background', 'engine/forms', 'engine/chain', 'entities/flower', 'entities/shutter', 'engine/save-state'],
	   function(moment, Consts, Background, Forms, Chain, Flower, Shutter, SaveState) {

	var SCALE_FLOWER_INIT = .15;
	var SCALE_FLOWER_DEMO = .25;

	var state = new Phaser.State();

    function playSound(id) {
    	if (!Consts.DEBUG.MUTE) {
    		if (state.voiceSound) {
    			state.voiceSound.stop();
    		}
	    	state.voiceSound = state.add.audio('minigame-' + id);
	    	state.voiceSound.play();
	    }
    }

    function playMusic(id) {
    	if (!Consts.DEBUG.MUTE) {
    		if (state.musicSound) {
    			state.musicSound.stop();
    		}
	    	state.musicSound = state.add.audio('minigame-' + id);
	    	state.musicSound.play('', 0, .8, true);
	    }
    }

    function click() {
    	state.add.audio('click').play();
    }

	state.preload = function() {
	};
	
	state.create = function() {
		this.playing = false;

		// Init assets

		playMusic('intro');

		this.bg = Background.create(this);

		this.dashed = this.add.image(180, 100, 'dashed'); 
		this.dashed.alpha = 0;

		var pot = this.add.image(Consts.WIDTH/2, 450, 'pot');
		pot.anchor.set(0.5);

		this.flower = Flower.create(this);
		this.shutter = Shutter.create(this);

		this.add.image(0, 0, 'minigame_frame');

		this.form = Forms.load('minigame');
		this.form.query('div').hide();
		this.form.show();

		this.chain = Chain.create();

		// Intro
		this.chain.add(function(next) {
			state.shutter.open(next);
		});
		this.chain.add(function(next) {
			state.flower.tween(SCALE_FLOWER_INIT, 1000, function() {
				setTimeout(next, 1000);
			})
		});

		// Help
		this.chain.add(function(next) {
			state.form.getElement('help-grow').fadeIn(200);
			playSound('up');
			state.upCallback = function() {
				state.upCallback = null;
				next();
			};
		});
		this.chain.add(function(next) {
			state.form.getElement('help-grow').hide();
			state.flower.tween(SCALE_FLOWER_DEMO, 500, function() {
				state.form.getElement('help-shrink').fadeIn(200);
				playSound('down');
				state.downCallback = function() {
					state.downCallback = null;
					next();
				};
			})
		});
		this.chain.add(function(next) {
			state.form.getElement('help-shrink').hide();
			state.flower.tween(SCALE_FLOWER_INIT, 500, function() {
				state.dashed.alpha = 1;
				state.form.getElement('help-goal').fadeIn(200);
				playSound('getready');
				setTimeout(function() {
					state.form.getElement('help-goal').fadeOut(200);
					next();
				}, 3500);
			});
		});

		// Countdown
		this.chain.add(function(next) {
			state.form.getElement('countdown').show();
			state.form.getElement('countdown').html('3');
			playSound('countdown');
			setTimeout(function() {
				state.form.getElement('countdown').html('2');
				setTimeout(function() {
					state.form.getElement('countdown').html('1');
					setTimeout(function() {
						next();
					}, 1000)
				}, 1000)
			}, 1000);
		});

		// Start
		this.chain.add(function(next) {
			playMusic('play');
			state.form.getElement('countdown').hide();
			state.playing = true;
			state.startTime = Date.now();
			state.form.getElement('timer').show();
			state.gameOverCallback = function() {
				state.gameOverCallback = null;
				state.playing = false;
				next();
			};
		});

		// You win!
		this.chain.add(function(next) {
			state.sound.stopAll();
			playSound('win');
			state.dashed.alpha = 0;
			state.form.getElement('victory').fadeIn(200);

			state.flash = state.add.image(Consts.WIDTH/2, Consts.HEIGHT/2, 'flash');
			state.flash.anchor.set(0.5);
			state.flash.scale.set(0.8);

			state.flash.rotateTween = state.add.tween(state.flash);
			state.flash.rotateTween.to({angle: 360}, 2000, null, true, true, false);
			
			//state.flower.tween(0.6, 500); // DEBUG
			state.world.bringToTop(state.flower.group);

			var up = state.game.input.keyboard.addKey(Phaser.Keyboard.UP)
			up.onDown.add(next);
			var down = state.game.input.keyboard.addKey(Phaser.Keyboard.DOWN)
			down.onDown.add(next);
		});

		this.chain.add(function(next) {
			state.form.hide();
			state.add.tween(state.flash).to({alpha: 0}, 500).start();
			state.shutter.close(next);
		})

		this.chain.add(function(next) {
			setTimeout(function() {
				state.game.state.start('menu');
			}, 1500);
		})

		this.chain.start();

    };

	state.update = function() {
		var up = this.input.keyboard.isDown(Phaser.KeyCode.UP);
		var down = this.input.keyboard.isDown(Phaser.KeyCode.DOWN);

		// Help callbacks
		if (this.upCallback && up) {
			click();
			this.upCallback();
		}
		if (this.downCallback && down) {
			click();
			this.downCallback();
		}

		// Game
		if (this.playing) {
			if (up) {
				this.flower.grow();
				if (this.flower.isReadyToRock() && this.gameOverCallback) {
					click();
					this.gameOverCallback();
				}
			}
			else if (down) {
				this.flower.shrink();
			}

			var time = moment(Date.now() - this.startTime);
			this.form.getElement('timer').html(time.format('mm:ss.SSS'));
		}
    };

	return state;

});
