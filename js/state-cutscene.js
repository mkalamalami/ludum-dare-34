define(['consts', 'background', 'entities/shutter', 'engine/forms', 'engine/tilemap', 'engine/chain', 'engine/save-state'], function(Consts, Background, Shutter, Forms, Tilemap, Chain, SaveState) {

	var state = new Phaser.State();

	state.preload = function() {
		this.levelInfo = {};
		this.levelInfo.tilesets = ["tileset", "objects"];
		this.load.tilemap('cutscene', 'tilemaps/maps/cutscene.json',
			null, Phaser.Tilemap.TILED_JSON);
	};

	state.create = function() {
		var outro = SaveState.victory;

		this.game.physics.startSystem(Phaser.Physics.P2JS);

		this.bg = Background.create(this);
		Tilemap.load(this, 'cutscene');
		this.add.image(-100, 202, 'throne');
		if (!outro) {
			var fruit = this.add.image(820, 340, 'mystic1');
			fruit.anchor.set(0.5,0.5);
			fruit.scale.set(0.8, 0.8);
		}
		var squ = this.add.image(650, 330, 'card1');
		squ.scale.set(-0.5, 0.5);

		this.chain = Chain.create();

		this.chain.add(function(next) {
			state.shutter = Shutter.create(state);
			state.shutter.cutsceneMode(function() {
				setTimeout(next, outro ? 500 : 2000);
			});

	    	state.musicSound = state.add.audio('cutscene');
	    	state.musicSound.fadeIn(2000, true);

		/*	state.add.tween(squ)
				.to({x: 600}, 2000, Phaser.Easing.Quadratic.InOut)
				.start();*/
			if (!outro) {
				state.add.tween(fruit.scale)
					.to({x: 0.9, y: 0.9}, 800,
						Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
			}
		});

		state.levelInfo.escKey = state.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
		state.levelInfo.escKey.onDown.addOnce(function() {
			state.chain.kill();
			state.musicSound.fadeOut(1000);
			if (state.form) {
				state.form.unload();
			}
			state.shutter.close(function() {
				if (outro) {
					state.game.state.start('menu');
				}
				else {
					state.game.state.start('level');
				}
			});
		});

		if (outro) {

			this.chain.add(function(next) {
				state.form = Forms.load('cutscene');
				state.form.hide();
				setText('The Mystical Berries are yours now, and you have proven your worthiness of their powers.', 5000, next);
			});

			this.chain.add(function(next) {
				state.form = Forms.load('cutscene');
				state.form.hide();
				setText('Listen everyone! As the shaman of this tribe, I declare without a doubt...', 4000, next);
			});

			this.chain.add(function(next) {
				state.form = Forms.load('cutscene');
				state.form.hide();
				setText('...that we\'ve finally found the Chosen One!', 3000, next);
			});

			this.chain.add(function(next) {
				state.form = Forms.load('cutscene');
				state.form.hide();
				setText('LONG LIVE THE QUEEN! LONG LIVE THE QUEEN!', 4000, next);
			});

		}
		else {
			this.chain.add(function(next) {
				state.form = Forms.load('cutscene');
				state.form.hide();
				setText('The day has come, my child.', 2500, next);
			});

			this.chain.add(function(next) {
				setText('Now is the time to show you have grown.<br />Now is the time to be the pride of your kin.', 6000, next);
			});

			this.chain.add(function(next) {
				setText('Go East, my child. Find all four Mystical Berries... The first one being right behind you.', 6000, next);
			});

			this.chain.add(function(next) {
				setText('Go now, go find the four Berries and master them!', 3000, next); // todo hourra
			});
		}

		this.chain.add(function(next) {
			state.musicSound.fadeOut(1000);
			state.form.hide(200, next);
		});

		this.chain.add(function(next) {
			state.shutter.close(next);
		});

		this.chain.add(function(next) {
			if (outro) {
				state.game.state.start('menu')
			}
			else {
				state.game.state.start('level');
			}
		});

		this.chain.start();

	};

	function setText(text, delay, callback) {
		state.form.hide(200, function() {
			state.form.getElement('text').html(text);
			setTimeout(function() {
				state.form.show(200, function() {
					setTimeout(callback, delay);
				});
			}, 500);
		})
	}

	return state;

});
