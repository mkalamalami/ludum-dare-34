define(['consts'], function(Consts) {

return {

	images: [
        'bgbottom',
        'bgtop',
        'dashed',
        'minigame_frame',
        'flower1',
        'flower2',
        'flower3',
        'flash',
        'shutter',
        'pot',
        'camera',
        'acorn',
        'acorn_ui',
        'bush',
        'stump',
        'stumpbig',
        'wall',
        'splinters',
        'mystic1',
        'mystic2',
        'mystic3',
        'mystic4',
        'key',
        'keysmall',
        'smoke',
        'throne',
        'logo',
        'waves',
		'dashed',
		'minigame_frame',
		'card1',
		'card2',
		'card3',
		'card4'
	],
	tilesets: [
		'tileset',
	    'objects'
	],
	audio: [
		'minigame-up',
		'minigame-down',
		'minigame-getready',
		'minigame-countdown',
		'minigame-win',
		'minigame-intro',
		'minigame-play',
                'start',
                'click',
                'call-squ',
                'call-foxy',
                'call-delta',
                'call-metal',
                'cutscene',
                'menu',
                'theme'
	],
	spritesheet: [
		['squ-walk', 150, 68],
        ['squ-jump', 148, 81],
        ['foxy-walk', 97, 44],
        ['foxy-roll', 46, 40],
        ['delta-walk', 150, 68],
        ['delta-fly', 150, 68],
        ['steel-walk', 200, 91],
        ['steel-brawl', 138, 120],
		['robot', 48, 96]
	]

};

});
