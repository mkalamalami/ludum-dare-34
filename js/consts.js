define([], function() {

	return {

		DEBUG : {
			SKIP_MINIGAME: false,
			SKIP_INTRO_CUTSCENE: false,
			SKIP_MENU: false,
			MUTE: false,
			SHOW_BODIES: false,
			DISABLE_SHUTTER: false,
			TEST_SUBMIT_LEADERBOARD: 0,
			FORCE_LEVEL: null
		},

		WIDTH: 960,
		HEIGHT: 600,
		TILESIZE: 96,

		SPEED: 400,
		GRAVITY: 2000,

		PLAYER_X: 300,
		LATE_PLAYER_ACCELERATION: 0.3,

		JUMP_FORCE: 900,
		
		COLLISION: {
			wall : {
				width: 170,
				height: 312,
				offset: {
					x: 0,
					y: -48
				}
			}
		},
		
		TERRAIN: {
			GROUND: "ground",
			AIR: "air",
			WATER: "water"
		},
		
		LEVELS: {
			0: {
				SUBLEVELS: 2
			},
			1: {
				SUBLEVELS: 1
			},
			2: {
				SUBLEVELS: 2
			},
			3: {
				SUBLEVELS: 2
			}
		},
		
		HTML: {
			KEYS: {
				1: "&#x21E7;",
				2: "&#x21E6;",
				3: "&#x21E8;",
				4: "&#x21E9;"
			}
		},
		SAVE_STATE_DEFAULT: {
			gameName: "save1",
        	playerName: "New player",
			level: 1,
			subLevel : 1,
			unlockedCharacters: 0,
			points: 0
        },

		SERVER_HOST: '176.31.244.164:8034'
		
	};

});
