define([], function() {

	return {
		correctObjectPosition: correctObjectPosition
	};

    function correctObjectPosition(sprite, yGrid, tileHeight, xGrid, tileWidth) {
        xGrid = (xGrid !== undefined) ? xGrid : false;
        yGrid = (yGrid !== undefined) ? yGrid : false;
        sprite.body.x += sprite.width/2;
		sprite.body.y -= sprite.height/2;
        
        if (xGrid) {
            sprite.body.x = Math.round(sprite.body.x / tileWidth) * tileWidth;
        }
        if (yGrid) {
            sprite.body.y = Math.round(sprite.body.y / tileHeight) * tileHeight;
			sprite.body.y += (tileHeight - (sprite.height % tileHeight)) / 2;
        }
    }
});
