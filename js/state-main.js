define(['consts', 'state-minigame', 'state-level', 'engine/save-state', 'engine/forms', 'leaderboards', 'state-menu', 'state-cutscene', 'state-leaderboard'],
		function(Consts, StateMiniGame, StateLevel, SaveState, Forms, Leaderboards, StateMenu, StateCutscene, StateLeaderboard) {

	var SAVENAME = 'save1';

	var state = new Phaser.State();

	state.preload = function() {

		// Save state

        SaveState.load(Consts.SAVE_STATE_DEFAULT.gameName, Consts.SAVE_STATE_DEFAULT);

		// Register game states

		this.state.add('minigame', StateMiniGame);
		this.state.add('cutscene', StateCutscene);
		this.state.add('menu', StateMenu);
		this.state.add('level', StateLevel);
		this.state.add('leaderboard', StateLeaderboard);

		// Start
		
		if (Consts.DEBUG.TEST_SUBMIT_LEADERBOARD != 0) {
			SaveState.points = Consts.DEBUG.TEST_SUBMIT_LEADERBOARD;
			this.state.start('leaderboard')
		}
		else if (!Consts.DEBUG.SKIP_MINIGAME && !SaveState.jumpToMenu) {
			this.state.start('minigame');
		}
		else if (!Consts.DEBUG.SKIP_MENU) {
			this.state.start('menu');
		}
		else if (!Consts.DEBUG.SKIP_INTRO_CUTSCENE) {
			this.state.start('cutscene');
		}
		else {
			this.state.start('level');
		}

	};

	state.create = function() {

	};

	return state;

});
